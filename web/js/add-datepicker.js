$(function(){
    $('.add-datepicker').each(function(i, el) {
        var options = (typeof $(el).data('options') == 'undefined') ? {}           : $(el).data('options');
        var widget  = (typeof $(el).data('widget')  == 'undefined') ? 'datepicker' : $(el).data('widget');

        if (widget == 'datepicker')
            $(el).datepicker(options);
        else if (widget == 'datetimepicker')
            $(el).datetimepicker(options);
    });
});