<?php

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
    public function getUserForWSResponse($id)
    {
        $query = $this->createQueryBuilder('U')
                ->where('U.id = :id')
                ->setParameter(':id', $id)
                ;
        
        $results = $query->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        if(count($results) > 0)
        {
            $user = $results[0];
            unset($user['password'], $user['plainPassword'], $user['salt']);
            
            return $user;
        }
        
        return array();
    }
}
