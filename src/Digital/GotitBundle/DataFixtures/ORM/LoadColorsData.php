<?php
/**
 * Created by PhpStorm.
 * User: Esteban
 * Date: 8/31/14
 * Time: 6:59 PM
 */

namespace Digital\GotitBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Digital\GotitBundle\Entity\Color;

class LoadColorsData implements FixtureInterface{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param Doctrine\Common\Persistence\ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        $blanco = new Color("Blanco", '#FFFFFF');
        $gris = new Color("Gris", '#A8B1B8');
        $plata = new Color("Plata", '#9EA8B2');
        $negro = new Color("Negro", '#000000');
        $amarillo = new Color("Amarillo", '#FFCF03');
        $amarilloMedio = new Color("Amarillo Medio", '#FFEB00');
        $amarilloLimon = new Color("Amarillo Limon", '#FFFF01');
        $azulAgua = new Color("Azul Agua", '#3BA7CD');
        $azulCielo = new Color("Azul Cielo", '#80BFF4');
        $azulRoyal = new Color("Azul Royal", '#004485');
        $azulSafiro = new Color("Azul Safiro", '#3595EB');
        $azulMarino = new Color("Azul Marino", '#363D59');

        $manager->persist($blanco);
        $manager->persist($gris);
        $manager->persist($plata);
        $manager->persist($negro);
        $manager->persist($amarillo);
        $manager->persist($amarilloMedio);
        $manager->persist($amarilloLimon);
        $manager->persist($azulAgua);
        $manager->persist($azulCielo);
        $manager->persist($azulRoyal);
        $manager->persist($azulSafiro);
        $manager->persist($azulMarino);

        $manager->flush();




    }
}