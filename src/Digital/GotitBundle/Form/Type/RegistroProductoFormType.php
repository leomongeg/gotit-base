<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Digital\GotitBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Description of RegistroProductoFormType
 *
 * @author esteban
 */
class RegistroProductoFormType extends AbstractType
{

    private $em;
    private $categoriaid;
    private $comercioid;
    private $marcaid;

    public function __construct($em/* , $categoriaid, $comercioid, $marcaid */)
    {
        $this->em = $em;
//        $this->categoriaid = $categoriaid;
//        $this->comercioid = $comercioid;
    }

    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     *
     * @throws \LogicException If DoctrineBundle is not available
     */
    public function getDoctrine()
    {
        return $this->em;
    }

    public function getComercio()
    {

        $repo = $this->getDoctrine()->getManager()->getRepository("GotitBundle:Comercio");
        $comercio = $repo->findOneById($this->comercioid);

        return $comercio;
    }

    public function getCategoriaProducto()
    {

        $repo = $this->getDoctrine()->getManager()->getRepository("GotitBundle:CategoriaProducto");
        $categoriaProducto = $repo->findOneById($this->categoriaid);

        return $categoriaProducto;
    }

    public function getMarca()
    {

        $repo = $this->getDoctrine()->getManager()->getRepository("GotitBundle:Marca");
        $marca = $repo->findOneById($this->marcaid);

        return $marca;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', 'text', array(
            'required' => true,
            'constraints' => array(new Assert\NotBlank()),
        ))
            ->add('codigo', 'number', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank()),
            ))
            ->add('cantidad', 'number', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank()),
            ))
            ->add('precio', 'number', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank()),
            ))
            ->add('categoriaProducto', null, array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank()),
            ))
            ->add('marca', null, array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank()),
            ))
            ->add('comercio', null, array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank()),
            ))

            ->add('file', 'file', array('mapped' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => 'Digital\GotitBundle\Entity\Producto',
//            'constraints'     => array(new Assert\Callback(array(array($this, 'establecerFecha'))))
        ));
    }

    public function getName()
    {
        return 'digital_gotit_registro_producto_rest';
    }

    public function establecerFecha($object, \Symfony\Component\Validator\ExecutionContextInterface $context)
    {
        $object->setFechaNacimiento(\DateTime::createFromFormat('Y-m-d', $object->getFechaNacimiento()));
    }

    /**
     * Verifica si el $value de $field se encuentra registrado en base de datos.
     *
     * @param string $field
     * @param string $value
     * @return boolean
     */
    private function isUniqueRequired($field, $value)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository("GotitBundle:Producto");
        $user = $repo->findOneBy(array($field => $value));

        return $user ? false : true;
    }

    /**
     * Si el valor de $field viene en el formulario. Si viene, se verifica si hay algun
     * registro con el mismo valor de $field.
     *
     * @param string $field Propiedad a validar
     * @param string $value Valore a buscar
     * @return boolean
     */
    private function isUniqueNotRequired($field, $value)
    {
        if (ctype_space($value) || $value != '') {
            $repo = $this->getDoctrine()->getManager()->getRepository("GotitBundle:Producto");
            $user = $repo->findOneBy(array($field => $value));

            return $user ? false : true;
        }

        return true;
    }

}
