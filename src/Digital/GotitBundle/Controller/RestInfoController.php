<?php
/**
 * Created by PhpStorm.
 * User: Esteban
 * Date: 9/4/14
 * Time: 8:31 PM
 */

namespace Digital\GotitBundle\Controller;

use FOS\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;


class RestInfoController extends Controller{


    /**
     * Get sucursales por comercio
     *
     * @Rest\Post("/rest/get_sucursales_comercio")
     * @Rest\View()
     *
     */
    public function getSucursalesComercioAction(){
        $request = $this->getRequest();
        $comercioId = $request->get('comercioId');
        $sucursales  = $this->getDoctrine()->getRepository('DigitalGotitBundle:Sucursal')->getSucursalesPorComercio($comercioId);
        return array("sucursales"=>$sucursales);
    }

    //hay que hacer el join para traer la relacion con productos de ese grupo
    /**
     * Get grupos de productos por comercio
     *
     * @Rest\Post("/rest/get_grupo_productos_comercio")
     * @Rest\View()
     *
     */
    public function getGrupoProductosComercioAction(){
        $request = $this->getRequest();
        $comercioId = $request->get('comercioId');
        $grupos  = $this->getDoctrine()->getRepository('DigitalGotitBundle:GrupoProductos')->getGruposPorComercio($comercioId);
        return array("grupoProductos"=>$grupos);
    }


    /**
     * Get productos de un comercio
     *
     * @Rest\Post("/rest/get_productos_comercio")
     * @Rest\View()
     *
     */
    public function getProductosComercioAction(){
        $request = $this->getRequest();
        $comercioId = $request->get('comercioId');
        $productos  = $this->getDoctrine()->getRepository('DigitalGotitBundle:Producto')->getProductosPorComercio($comercioId);
        return array("productos"=>$productos);
    }

    /**
     * Get the Create Producto
     *
     * @Rest\Post("/rest/portal/create_producto")
     * @Rest\View()
     */
    public function newProductoAction() {
        $form = new \Lumen\AscanBundle\Form\Type\RegistroProductoFormType($this->getDoctrine());
        $form = $this->createForm($form, new Producto());
        $errors = array();
        if (!$producto = $this->processForm($form, $errors)) {
            return array('msn' => 'Error', "info" => $errors);
        }

        $producto->setComercio($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $em->persist($producto);
        $em->flush();
        $baseUrl = 'http://' . $this->getRequest()->getHost() . '/' . $producto->getUploadDir() . '/';

        $response = $em->getRepository('GotitBundle:Producto')->getProductosComercioWS($producto->getComercio()->getId(), $baseUrl);
        return array('msn' => 'OK', 'info' => $response);
    }

    /**
     * Get productos de un comercio
     *
     */
    public function getProductosComercioWS($comerId){
        $comercioId = $comerId;
        $productos  = $this->getDoctrine()->getRepository('DigitalGotitBundle:Producto')->getProductosPorComercio($comercioId);
        return array("productos"=>$productos);
    }

}