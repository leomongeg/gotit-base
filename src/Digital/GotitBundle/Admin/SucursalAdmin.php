<?php


namespace Digital\GotitBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class SucursalAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();

            $formMapper
                    ->add('nombre', 'text', array('label' => 'Nombre', 'required' => true))
                    ->add('telefono', 'text', array('label' => 'Telefono', 'required' => true))
                    ->add('direccion', 'text', array('label' => 'Direccion', 'required' => true))
                    ->add('provincia', 'text', array('label' => 'Provincia', 'required' => true))
                    ->add('distrito', 'text', array('label' => 'Distrito', 'required' => true))
                    ->add('latlng', 'oh_google_maps')
                    ->add('comercio', 'entity', array('class' => 'Digital\GotitBundle\Entity\Comercio'));
            ;

    }

    public function prePersist($sucursal) {
        
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('nombre')
                ->add('telefono')
                ->add('direccion')
/*                ->add('latitud')
                ->add('longitud')*/
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('nombre')
                ->add('telefono')
                ->add('direccion')
//                ->add('latitud')
//                ->add('longitud')
        ;
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list') {
        $query = $this->getModelManager()->createQuery('Digital\GotitBundle\Entity\Sucursal', 's');

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        if ($user->getUsername() != 'admin' && $context == 'list') {
            $query->andWhere('s.cadena = ' . $user->getCadena()->getId());
        }
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

}
