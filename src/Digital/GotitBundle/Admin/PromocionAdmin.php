<?php
/**
 * Created by PhpStorm.
 * User: Esteban
 * Date: 8/31/14
 * Time: 2:04 PM
 */

namespace Digital\GotitBundle\Admin;



use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PromocionAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('titulo', 'text', array('label' => 'Titulo', 'required' => true))
            ->add('file', 'file', array('required' => false))
            ->add('porcentajeDescuento', 'number', array('label' => 'Porcentaje de descuento', 'required' => true))
            ->add('fechaInicio', 'date', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => array('class' => 'add-datepicker',
                    'data-options' => '{ "changeMonth": true, "changeYear": true, "dateFormat": "yy-mm-dd" }')
            ))
            ->add('fechaCaducidad', 'date', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => array('class' => 'add-datepicker',
                    'data-options' => '{ "changeMonth": true, "changeYear": true, "dateFormat": "yy-mm-dd" }')
            ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titulo')
            ->add('porcentajeDescuento')
            ->add('fechaInicio')
            ->add('fechaCaducidad')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('titulo')
            ->add('porcentajeDescuento')
            ->add('fechaInicio')
            ->add('fechaCaducidad')
        ;
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

}
