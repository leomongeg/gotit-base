<?php
/**
 * Created by PhpStorm.
 * User: Esteban
 * Date: 8/31/14
 * Time: 1:54 PM
 */

namespace Digital\GotitBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class ProductoAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('nombre', 'text', array('label' => 'Nombre', 'required' => true))
            ->add('file', 'file', array('required' => false))
            ->add('codigo', 'number', array('label' => 'Codigo', 'required' => true))
            ->add('cantidad', 'number', array('label' => 'Cantidad', 'required' => true))
            ->add('colores')
           // ->add('color', 'entity', array('class' => 'Digital\GotitBundle\Entity\Color'))
            ->add('precio', 'number', array('label' => 'Precio', 'required' => true))
            ->add('marca', 'entity', array('class' => 'Digital\GotitBundle\Entity\Marca'))
            ->add('categoriaProducto', 'entity', array('class' => 'Digital\GotitBundle\Entity\CategoriaProducto'))
            ->add('comercio', 'entity', array('class' => 'Digital\GotitBundle\Entity\Comercio'))
        ;
    }

    public function prePersist($object) {

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('nombre')
            ->add('codigo')
            ->add('cantidad')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->add('nombre')
            ->add('codigo')
            ->add('cantidad')

        ;
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list') {
        $query = $this->getModelManager()->createQuery('Digital\GotitBundle\Entity\Producto', 'c');

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        if ($user->getUsername() != 'admin' && $context == 'list') {
            $query->andWhere('c.id = ' . $user->getCadena()->getId());
        }
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

}