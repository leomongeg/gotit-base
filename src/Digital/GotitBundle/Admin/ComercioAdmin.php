<?php
/**
 * Created by PhpStorm.
 * User: Esteban
 * Date: 8/31/14
 * Time: 1:54 PM
 */

namespace Digital\GotitBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class ComercioAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {

        $formMapper
            ->add('nombre', 'text', array('label' => 'Nombre', 'required' => true))
            ->add('file', 'file', array('required' => false))
            ->add('cedulaJuridica', 'text', array('label' => 'Cedula Juridica', 'required' => true))
            ->add('mision', 'text', array('label' => 'Mision', 'required' => true))
            ->add('vision', 'text', array('label' => 'Vision', 'required' => true))
            ->add('descripcion', 'text', array('label' => 'Descripcion', 'required' => true))
            ->add('direccionOficinaCentral', 'text', array('label' => 'Direccion Ofi.Central', 'required' => true))
            ->add('telefonoOficinaCentral', 'text', array('label' => 'Telefono Ofi.Central', 'required' => true))
            ->add('encargado', 'text', array('label' => 'Encargado', 'required' => true))
            ->add('urlEmpresarial', 'text', array('label' => 'Sitio web', 'required' => true))
            ->add('categoriaComercio', 'entity', array('class' => 'Digital\GotitBundle\Entity\CategoriaComercio'))
        ;
    }

    public function prePersist($object) {

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('nombre')
            ->add('cedulaJuridica')
            ->add('encargado')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->add('nombre')
            ->add('cedulaJuridica')
            ->add('encargado')

        ;
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list') {
        $query = $this->getModelManager()->createQuery('Digital\GotitBundle\Entity\Comercio', 'c');

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        if ($user->getUsername() != 'admin' && $context == 'list') {
            $query->andWhere('c.id = ' . $user->getCadena()->getId());
        }
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

}