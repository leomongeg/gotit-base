<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriaProducto
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class CategoriaProducto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="categoriaProducto")
     */
    protected $productos;



    /**
     * Constructor
     */
    public function __construct() {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CategoriaProducto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     * @return CategoriaProducto
     */
    public function addComercio(\Digital\GotitBundle\Entity\Comercio $productos) {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     */
    public function removeProducto(\Digital\GotitBundle\Entity\Producto $productos) {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductos() {
        return $this->productos;
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Add productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     * @return CategoriaProducto
     */
    public function addProducto(\Digital\GotitBundle\Entity\Producto $productos)
    {
        $this->productos[] = $productos;
    
        return $this;
    }
}