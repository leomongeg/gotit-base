<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

define('PROMOCION_IMAGE_PATH', 'promocion');

/**
 * Promocion
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Promocion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="porcentajeDescuento", type="integer")
     */
    private $porcentajeDescuento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCaducidad", type="datetime")
     */
    private $fechaCaducidad;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagen;


    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * ORM\Column(type="string", length=255, nullable=true)
     */
//    public $path;

    private $tempPath;

    public function getAbsolutePath()
    {
        return null === $this->imagen ? null : $this->getUploadRootDir() . '/' . $this->imagen;
    }

    public function getWebPath()
    {
        return null === $this->imagen ? null : $this->getUploadDir() . '/' . $this->imagen;
    }

    public function getUploadRootDir()
    {
        // the absolute directory imagen where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/' . PROMOCION_IMAGE_PATH;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image imagen
        if (isset($this->imagen))
        {
            // store the old name to delete after the update
            $this->tempPath = $this->imagen;
            $this->imagen = null;
        } else
        {
            $this->imagen = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    final public function preUploadEntityImage()
    {
        if (null !== $this->getFile())
        {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imagen = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    final public function uploadEntityImage()
    {
        if (null === $this->getFile())
        {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->imagen);

        // check if we have an old image
        if (isset($this->tempPath))
        {
            // delete the old image
            @unlink($this->getUploadRootDir() . '/' . $this->tempPath);
            // clear the temp image imagen
            $this->tempPath = null;
        }

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    final public function removeUploadEntityImage()
    {
        if ($file = $this->getAbsolutePath())
        {
            if ($this->imagen)
            {
                unlink($file);
            }
        }
    }

    public $fotoUrl;

    public function getFotoUrl()
    {
        return $this->fotoUrl;
    }

    public function getPictureName()
    {
        return $this->imagen;
    }

// </editor-fold>


    /**
     * Constructor
     */
    public function __construct() {

        $this->imagen = '';
        $this->fotoUrl = '';
    }


    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Promocion
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Promocion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set porcentajeDescuento
     *
     * @param integer $porcentajeDescuento
     * @return Promocion
     */
    public function setPorcentajeDescuento($porcentajeDescuento)
    {
        $this->porcentajeDescuento = $porcentajeDescuento;
    
        return $this;
    }

    /**
     * Get porcentajeDescuento
     *
     * @return integer 
     */
    public function getPorcentajeDescuento()
    {
        return $this->porcentajeDescuento;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Promocion
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    
        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaCaducidad
     *
     * @param \DateTime $fechaCaducidad
     * @return Promocion
     */
    public function setFechaCaducidad($fechaCaducidad)
    {
        $this->fechaCaducidad = $fechaCaducidad;
    
        return $this;
    }

    /**
     * Get fechaCaducidad
     *
     * @return \DateTime 
     */
    public function getFechaCaducidad()
    {
        return $this->fechaCaducidad;
    }

}