<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\EntityRepository;


class SucursalRepository extends EntityRepository {

    static $sucursalesCollection;
    

    public function getSucursales($comercioId = 0) {


        $dql = 'SELECT S.id, C.nombre as comercioNombre, C.id as comercioId, C.logoEmpresarial as logoEmpresarial, S.nombre, S.telefono, S.direccion, S.telefono, S.lat, S.lng '
            . 'FROM DigitalGotitBundle:Sucursal S '
            . 'INNER JOIN S.comercio C ';


        if ($comercioId != 0) {
            $dql = $dql . 'WHERE S.comercio = :comercioId ';
        }

        $dql = $dql . 'ORDER BY S.comercio ASC';

        $query = $this->getEntityManager()->createQuery($dql);
        if ($comercioId != 0) {
            $query->setParameter(':comercioId', $comercioId);
        }

        $results = $query->getResult();
       // var_dump($results);die;
        $collection = array();
        foreach ($results as $key => $value) {
            $filaValor = array('id' => $value['id'], 'comercio_id' => $value['comercioId'], 'comercio' => $value['comercioNombre'], 'logoEmpresarial' => $value['logoEmpresarial'], 'nombre' => $value['nombre'], 'telefono' => $value['telefono'], 'direccion' => $value['direccion'], 'lat' => $value['lat'], 'lng' => $value['lng']);

            $collection[] = $filaValor;
        }
        unset($results);
        self::$sucursalesCollection = $collection;


        return self::$sucursalesCollection;
    }

    public function getSucursalesPorComercio($cadenaId = 0) {

        $sucursalesPorCadena = $this->getSucursales($cadenaId);

        return $sucursalesPorCadena;
    }
    


}
