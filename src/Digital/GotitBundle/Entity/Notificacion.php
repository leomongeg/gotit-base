<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Notificacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Digital\GotitBundle\Entity\NotificacionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Notificacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="text")
     */
    protected $texto;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    protected $tipo;

    /**
     * @ORM\OneToOne(targetEntity="Beacon", inversedBy="notificacion")
     * @ORM\JoinColumn(name="beacon_id", referencedColumnName="id", nullable=true)
     */
    protected $beacon;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Notificacion
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;
    
        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Notificacion
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set beacon
     *
     * @param \Digital\GotitBundle\Entity\Beacon $beacon
     * @return Notificacion
     */
    public function setBeacon(\Digital\GotitBundle\Entity\Beacon $beacon = null)
    {
        $this->beacon = $beacon;
    
        return $this;
    }

    /**
     * Get beacon
     *
     * @return \Digital\GotitBundle\Entity\Beacon 
     */
    public function getBeacon()
    {
        return $this->beacon;
    }
}