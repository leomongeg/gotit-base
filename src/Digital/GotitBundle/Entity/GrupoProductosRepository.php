<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\EntityRepository;


class GrupoProductosRepository extends EntityRepository {

    static $grupoProductosCollection;
    

    public function getGrupos($comercioId = 0) {


        $dql = 'SELECT GP.id, C.nombre as comercioNombre, C.id as comercioId, GP.nombre '
            . 'FROM DigitalGotitBundle:GrupoProductos GP '
            . 'INNER JOIN GP.comercio C ';


        if ($comercioId != 0) {
            $dql = $dql . 'WHERE GP.comercio = :comercioId ';
        }

        $dql = $dql . 'ORDER BY GP.comercio ASC';

        $query = $this->getEntityManager()->createQuery($dql);
        if ($comercioId != 0) {
            $query->setParameter(':comercioId', $comercioId);
        }

        $results = $query->getResult();
       // var_dump($results);die;
        $collection = array();
        foreach ($results as $key => $value) {
            $filaValor = array('id' => $value['id'], 'comercio_id' => $value['comercioId'], 'comercio' => $value['comercioNombre'], 'nombre' => $value['nombre']);

            $collection[] = $filaValor;
        }
        unset($results);
        self::$grupoProductosCollection = $collection;


        return self::$grupoProductosCollection;
    }

    public function getGruposPorComercio($cadenaId = 0) {

        $gruposPorCadena = $this->getGrupos($cadenaId);

        return $gruposPorCadena;
    }
    


}
