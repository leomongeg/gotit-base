<?php

namespace Digital\GotitBundle\Entity;

use Digital\GotitBundle\Admin\CategoriaProductoAdmin;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

define('PRODUCTO_IMAGE_PATH', 'producto');

/**
 * Producto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Digital\GotitBundle\Entity\ProductoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo", type="integer", nullable=true)
     */
    private $codigo;

    /**
     * @ORM\ManyToOne(targetEntity="Digital\GotitBundle\Entity\Marca", inversedBy="productos")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     */
    protected $marca;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;


    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="decimal")
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenProducto", type="string", length=255, nullable=true)
     */
    private $imagenProducto;

// <editor-fold defaultstate="collapsed" desc="Upload Image Logo Empresarial">

    //agrego relacion
    /**
     * @ORM\ManyToOne(targetEntity="Digital\GotitBundle\Entity\Comercio", inversedBy="productos")
     * @ORM\JoinColumn(name="comercio_id", referencedColumnName="id")
     */
    protected $comercio;

    /**
     * @ORM\ManyToOne(targetEntity="CategoriaProducto", inversedBy="producto")
     * @ORM\JoinColumn(name="categoriaproducto_id", referencedColumnName="id")
     */
    protected $categoriaProducto;


    /**
     * @ORM\ManyToMany(targetEntity="Color", inversedBy="productos")
     * @ORM\JoinTable(name="productos_colores")
     */
    protected $colores;

    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $comercio_id;

    //agrego relacion
    /**
     * @ORM\ManyToMany(targetEntity="GrupoProductos", inversedBy="productos")
     * @ORM\JoinTable(name="digital_productos_grupo")
     */
    protected $grupoproductos;

    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $grupoproductos_id;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * ORM\Column(type="string", length=255, nullable=true)
     */
//    public $path;

    private $tempPath;

    public function getAbsolutePath()
    {
        return null === $this->imagenProducto ? null : $this->getUploadRootDir() . '/' . $this->imagenProducto;
    }

    public function getWebPath()
    {
        return null === $this->imagenProducto ? null : $this->getUploadDir() . '/' . $this->imagenProducto;
    }

    public function getUploadRootDir()
    {
        // the absolute directory imagen where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/' . PRODUCTO_IMAGE_PATH;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image imagen
        if (isset($this->imagenProducto))
        {
            // store the old name to delete after the update
            $this->tempPath = $this->imagenProducto;
            $this->imagenProducto = null;
        } else
        {
            $this->imagenProducto = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    final public function preUploadEntityImage()
    {
        if (null !== $this->getFile())
        {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imagenProducto = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    final public function uploadEntityImage()
    {
        if (null === $this->getFile())
        {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->imagenProducto);

        // check if we have an old image
        if (isset($this->tempPath))
        {
            // delete the old image
            @unlink($this->getUploadRootDir() . '/' . $this->tempPath);
            // clear the temp image imagen
            $this->tempPath = null;
        }

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    final public function removeUploadEntityImage()
    {
        if ($file = $this->getAbsolutePath())
        {
            if ($this->imagenProducto)
            {
                unlink($file);
            }
        }
    }

    public $fotoUrl;

    public function getFotoUrl()
    {
        return $this->fotoUrl;
    }

    public function getPictureName()
    {
        return $this->imagenProducto;
    }

// </editor-fold>


    /**
     * Constructor
     */
    public function __construct() {
        $this->colores  = new \Doctrine\Common\Collections\ArrayCollection();
        $this->imagenProducto = '';
        $this->fotoUrl = '';
    }




    public function setComercioId($comercio_id) {
        $this->comercio_id = $comercio_id;

        return $this;
    }

    public function getComercioId() {
        return $this->comercio_id;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set codigo
     *
     * @param integer $codigo
     * @return Producto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set marca
     *
     * @param \Digital\GotitBundle\Entity\Marca $marca
     * @return Producto
     */
    public function setMarca( \Digital\GotitBundle\Entity\Marca $marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \Digital\GotitBundle\Entity\Marca
     */
    public function getMarca()
    {
        return $this->marca;
    }


    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return Producto
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }


    /**
     * Set precio
     *
     * @param float $precio
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set imagenProducto
     *
     * @param string $imagenProducto
     * @return Producto
     */
    public function setImagenProducto($imagenProducto)
    {
        $this->imagenProducto = $imagenProducto;

        return $this;
    }

    /**
     * Get imagenProducto
     *
     * @return string
     */
    public function getImagenProducto()
    {
        return $this->imagenProducto;
    }



    /**
     * Set comercio
     *
     * @param \Digital\GotitBundle\Entity\Comercio $comercio
     * @return Producto
     */
    public function setComercio( \Digital\GotitBundle\Entity\Comercio $comercio)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \Digital\GotitBundle\Entity\Comercio
     */
    public function getComercio()
    {
        return $this->comercio;
    }

    /**
     * Set categoriaProducto
     *
     * @param CategoriaProducto $categoriaProducto
     * @return Producto
     */
    public function setCategoriaProducto(\Digital\GotitBundle\Entity\CategoriaProducto $categoriaProducto)
    {
        $this->categoriaProducto = $categoriaProducto;

        return $this;
    }

    /**
     * Get categoriaProducto
     *
     * @return CategoriaProducto
     */
    public function getCategoriaProducto()
    {
        return $this->categoriaProducto;
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Set grupoproductos_id
     *
     * @param integer $grupoproductosId
     * @return Producto
     */
    public function setGrupoproductosId($grupoproductosId)
    {
        $this->grupoproductos_id = $grupoproductosId;
    
        return $this;
    }

    /**
     * Get grupoproductos_id
     *
     * @return integer 
     */
    public function getGrupoproductosId()
    {
        return $this->grupoproductos_id;
    }

    /**
     * Add colores
     *
     * @param \Digital\GotitBundle\Entity\Color $colores
     * @return Producto
     */
    public function addColores(\Digital\GotitBundle\Entity\Color $colores)
    {
        $this->colores[] = $colores;
    
        return $this;
    }

    /**
     * Remove colores
     *
     * @param \Digital\GotitBundle\Entity\Color $colores
     */
    public function removeColores(\Digital\GotitBundle\Entity\Color $colores)
    {
        $this->colores->removeElement($colores);
    }

    /**
     * Get colores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColores()
    {
        return $this->colores;
    }


    /**
     * Add colores
     *
     * @param \Digital\GotitBundle\Entity\Color $colores
     * @return Producto
     */
    public function addColore(\Digital\GotitBundle\Entity\Color $colores)
    {
        $this->colores[] = $colores;
    
        return $this;
    }

    /**
     * Remove colores
     *
     * @param \Digital\GotitBundle\Entity\Color $colores
     */
    public function removeColore(\Digital\GotitBundle\Entity\Color $colores)
    {
        $this->colores->removeElement($colores);
    }

    /**
     * Add grupoproductos
     *
     * @param \Digital\GotitBundle\Entity\GrupoProductos $grupoproductos
     * @return Producto
     */
    public function addGrupoproducto(\Digital\GotitBundle\Entity\GrupoProductos $grupoproductos)
    {
        $this->grupoproductos[] = $grupoproductos;
    
        return $this;
    }

    /**
     * Remove grupoproductos
     *
     * @param \Digital\GotitBundle\Entity\GrupoProductos $grupoproductos
     */
    public function removeGrupoproducto(\Digital\GotitBundle\Entity\GrupoProductos $grupoproductos)
    {
        $this->grupoproductos->removeElement($grupoproductos);
    }

    /**
     * Get grupoproductos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupoproductos()
    {
        return $this->grupoproductos;
    }
}