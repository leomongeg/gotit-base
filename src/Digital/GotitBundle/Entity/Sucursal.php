<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;


/**
 * Sucursal
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Digital\GotitBundle\Entity\SucursalRepository")
 */
class Sucursal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;


    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=255)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="distrito", type="string", length=255)
     */
    private $distrito;





    //agrego relacion
    /**
     * @ORM\ManyToOne(targetEntity="Digital\GotitBundle\Entity\Comercio", inversedBy="sucursales")
     * @ORM\JoinColumn(name="comercio_id", referencedColumnName="id")
     */
    protected $comercio;

    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $comercio_id;

    //se agrego mapeo
    /**
     * @ORM\OneToMany(targetEntity="Beacon", mappedBy="Comercio")
     */
    protected $beacons;
    
    /**
     * @ORM\OneToMany(targetEntity="GrupoProductos", mappedBy="sucursal", cascade={"all"}, orphanRemoval=true)
     * @var type 
     */
    protected $grupoProductos;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="float", length=255)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="float", length=255)
     */
    private $lng;

    /**
     * @Assert\NotBlank()
     * @OhAssert\LatLng()
     */
    public function getLatLng() {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Sucursal
     */
    public function setLat($lat) {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat() {
        return $this->lat;
    }

    public function setLatLng($latlng) {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);
        return $this;
    }

    /**
     * Set lng
     *
     * @param string $longitud
     * @return Sucursal
     */
    public function setLng($lng) {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng() {
        return $this->lng;
    }


    /**
     * Constructor
     */
    public function __construct() {
        $this->beacons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setComercioId($comercio_id) {
        $this->comercio_id = $comercio_id;

        return $this;
    }

    public function getComercioId() {
        return $this->comercio_id;
    }

    /**
     * Set comercio
     *
     * @param \Digital\GotitBundle\Entity\Comercio $comercio
     * @return Comercio
     */
    public function setComercio(\Digital\GotitBundle\Entity\Comercio $comercio = null) {
        $this->comercio = $comercio;

        return $this;
    }


    /**
     * Get comercio
     *
     * @return \Digital\GotitBundle\Entity\Comercio
     */
    public function getComercio() {
        return $this->comercio;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Sucursal
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Sucursal
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Sucursal
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     * @return Sucursal
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set canton
     *
     * @param string $distrito
     * @return Sucursal
     */
    public function setDistrito($distrito)
    {
        $this->distrito = $distrito;

        return $this;
    }

    /**
     * Get distrito
     *
     * @return string 
     */
    public function getDistrito()
    {
        return $this->distrito;
    }


    /**
     * Add beacons
     *
     * @param \Digital\GotitBundle\Entity\Beacon $beacons
     * @return Beacon
     */
    public function addBeacon(\Digital\GotitBundle\Entity\Beacon $beacons) {
        $this->beacons[] = $beacons;

        return $this;
    }

    /**
     * Remove beacons
     *
     * @param \Digital\GotitBundle\Entity\Beacon $beacons
     */
    public function removeBeacon(\Digital\GotitBundle\Entity\Beacon $beacons) {
        $this->beacons->removeElement($beacons);
    }

    /**
     * Get beacons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeacons() {
        return $this->beacons;
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Add grupoProductos
     *
     * @param \Digital\GotitBundle\Entity\GrupoProductos $grupoProductos
     * @return Sucursal
     */
    public function addGrupoProducto(\Digital\GotitBundle\Entity\GrupoProductos $grupoProductos)
    {
        $this->grupoProductos[] = $grupoProductos;
    
        return $this;
    }

    /**
     * Remove grupoProductos
     *
     * @param \Digital\GotitBundle\Entity\GrupoProductos $grupoProductos
     */
    public function removeGrupoProducto(\Digital\GotitBundle\Entity\GrupoProductos $grupoProductos)
    {
        $this->grupoProductos->removeElement($grupoProductos);
    }

    /**
     * Get grupoProductos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupoProductos()
    {
        return $this->grupoProductos;
    }
}