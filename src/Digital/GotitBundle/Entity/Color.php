<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Color
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Color
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="hexadecimal", type="string", length=255)
     */
    private $hexadeciamal;


    /**
     * @ORM\ManyToMany(targetEntity="Producto", mappedBy="colores")
     * */
    private $productos;


    /**
     * Constructor
     */
    public function __construct($nombre, $hexa) {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
        if($nombre != "" && $hexa !=""){
            $this->setNombre($nombre);
            $this->setHexadeciamal($hexa);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Marca
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set hexadeciamal
     *
     * @param string $hexadeciamal
     * @return Color
     */
    public function setHexadeciamal($hexadeciamal)
    {
        $this->hexadeciamal = $hexadeciamal;

        return $this;
    }

    /**
     * Get hexadeciamal
     *
     * @return string
     */
    public function getHexadeciamal()
    {
        return $this->hexadeciamal;
    }


    /**
     * Add productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     * @return Color
     */
    public function addProducto(\Digital\GotitBundle\Entity\Producto $productos) {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $comercios
     */
    public function removeProducto(\Digital\GotitBundle\Entity\Producto $productos) {
        $this->productos->removeElement($productos);
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}