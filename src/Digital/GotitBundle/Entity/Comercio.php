<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


define('COMERCIO_IMAGE_PATH', 'comercio');
/**
 * Comercio
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Comercio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="cedulaJuridica", type="string", length=255)
     */
    private $cedulaJuridica;

    /**
     * @var string
     *
     * @ORM\Column(name="direccionOficinaCentral", type="string", length=255)
     */
    private $direccionOficinaCentral;


    /**
     * @var string
     *
     * @ORM\Column(name="telefonoOficinaCentral", type="string", length=255)
     */
    private $telefonoOficinaCentral;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="mision", type="string", length=500)
     */
    private $mision;

    /**
     * @var string
     *
     * @ORM\Column(name="vision", type="string", length=500)
     */
    private $vision;

    /**
     * @var string
     *
     * @ORM\Column(name="encargado", type="string", length=255)
     */
    private $encargado;

    /**
     * @var string
     *
     * @ORM\Column(name="urlEmpresarial", type="string", length=400, nullable=true)
     */
    private $urlEmpresarial;

    /**
     * @ORM\ManyToOne(targetEntity="CategoriaComercio", inversedBy="comercios")
     * @ORM\JoinColumn(name="categoriacomercio_id", referencedColumnName="id")
     */
    protected $categoriaComercio;


    //se agrego mapeo
    /**
     * @ORM\OneToMany(targetEntity="Sucursal", mappedBy="Comercio")
     */
    protected $sucursales;

    //se agrego mapeo
    /**
     * @ORM\OneToMany(targetEntity="Beacon", mappedBy="Comercio")
     */
    protected $beacons;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="comercio", cascade={"all"}, orphanRemoval=true)
     */
    protected $productos;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logoEmpresarial;


// <editor-fold defaultstate="collapsed" desc="Upload Image Logo Empresarial">
    

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * ORM\Column(type="string", length=255, nullable=true)
     */
//    public $path;
    
    private $tempPath;

    public function getAbsolutePath()
    {
        return null === $this->logoEmpresarial ? null : $this->getUploadRootDir() . '/' . $this->logoEmpresarial;
    }

    public function getWebPath()
    {
        return null === $this->logoEmpresarial ? null : $this->getUploadDir() . '/' . $this->logoEmpresarial;
    }

    public function getUploadRootDir()
    {
        // the absolute directory imagen where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/' . COMERCIO_IMAGE_PATH;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image imagen
        if (isset($this->logoEmpresarial))
        {
            // store the old name to delete after the update
            $this->tempPath = $this->logoEmpresarial;
            $this->logoEmpresarial = null;
        } else
        {
            $this->logoEmpresarial = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    final public function preUploadEntityImage()
    {
        if (null !== $this->getFile())
        {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->logoEmpresarial = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    final public function uploadEntityImage()
    {
        if (null === $this->getFile())
        {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->logoEmpresarial);

        // check if we have an old image
        if (isset($this->tempPath))
        {
            // delete the old image
            @unlink($this->getUploadRootDir() . '/' . $this->tempPath);
            // clear the temp image imagen
            $this->tempPath = null;
        }

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    final public function removeUploadEntityImage()
    {
        if ($file = $this->getAbsolutePath())
        {
            if ($this->logoEmpresarial)
            {
                unlink($file);
            }
        }
    }

    public $fotoUrl;

    public function getFotoUrl()
    {
        return $this->fotoUrl;
    }

    public function getPictureName()
    {
        return $this->logoEmpresarial;
    }
    
    
    
    
// </editor-fold>
   

    /**
     * Constructor
     */
    public function __construct() {
        $this->sucursales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->beacons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->gruposproducto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->logoEmpresarial = '';
        $this->fotoUrl = '';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Comercio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cedulaJuridica
     *
     * @param string $cedulaJuridica
     * @return Comercio
     */
    public function setCedulaJuridica($cedulaJuridica)
    {
        $this->cedulaJuridica = $cedulaJuridica;

        return $this;
    }

    /**
     * Get cedulaJuridica
     *
     * @return string 
     */
    public function getCedulaJuridica()
    {
        return $this->cedulaJuridica;
    }

    /**
     * Set direccionOficinaCentral
     *
     * @param string $direccionOficinaCentral
     * @return Comercio
     */
    public function setDireccionOficinaCentral($direccionOficinaCentral)
    {
        $this->direccionOficinaCentral = $direccionOficinaCentral;

        return $this;
    }

    /**
     * Get direccionOficinaCentral
     *
     * @return string 
     */
    public function getDireccionOficinaCentral()
    {
        return $this->direccionOficinaCentral;
    }

    /**
     * Set categoriaComercio
     *
     * @param string $categoriaComercio
     * @return Comercio
     */
    public function setCategoriaComercio(\Digital\GotitBundle\Entity\CategoriaComercio $categoriaComercio)
    {
        $this->categoriaComercio = $categoriaComercio;

        return $this;
    }

    /**
     * Get categoriaComercio
     *
     * @return string 
     */
    public function getCategoriaComercio()
    {
        return $this->categoriaComercio;
    }

    /**
     * Set logoEmpresarial
     *
     * @param string $logoEmpresarial
     * @return Comercio
     */
    public function setLogoEmpresarial($logoEmpresarial)
    {
        $this->logoEmpresarial = $logoEmpresarial;

        return $this;
    }

    /**
     * Get logoEmpresarial
     *
     * @return string 
     */
    public function getLogoEmpresarial()
    {
        return $this->logoEmpresarial;
    }

    /**
     * Set telefonoOficinaCentral
     *
     * @param string $telefonoOficinaCentral
     * @return Comercio
     */
    public function setTelefonoOficinaCentral($telefonoOficinaCentral)
    {
        $this->telefonoOficinaCentral = $telefonoOficinaCentral;

        return $this;
    }

    /**
     * Get telefonoOficinaCentral
     *
     * @return string 
     */
    public function getTelefonoOficinaCentral()
    {
        return $this->telefonoOficinaCentral;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Comercio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set mision
     *
     * @param string $mision
     * @return Comercio
     */
    public function setMision($mision)
    {
        $this->mision = $mision;

        return $this;
    }

    /**
     * Get mision
     *
     * @return string
     */
    public function getMision()
    {
        return $this->mision;
    }

    /**
     * Set vision
     *
     * @param string $vision
     * @return Comercio
     */
    public function setVision($vision)
    {
        $this->vision = $vision;

        return $this;
    }

    /**
     * Get vision
     *
     * @return string
     */
    public function getVision()
    {
        return $this->vision;
    }

    /**
     * Set encargado
     *
     * @param string $encargado
     * @return Comercio
     */
    public function setEncargado($encargado)
    {
        $this->encargado = $encargado;

        return $this;
    }

    /**
     * Get encargado
     *
     * @return string 
     */
    public function getEncargado()
    {
        return $this->encargado;
    }


    /**
     * Set urlEmpresarial
     *
     * @param string $urlEmpresarial
     * @return urlEmpresarial
     */
    public function setUrlEmpresarial($urlEmpresarial)
    {
        $this->urlEmpresarial = $urlEmpresarial;

        return $this;
    }

    /**
     * Get urlEmpresarial
     *
     * @return string
     */
    public function getUrlEmpresarial()
    {
        return $this->urlEmpresarial;
    }



    /**
     * Add sucursales
     *
     * @param \Digital\GotitBundle\Entity\Sucursal $sucursales
     * @return Sucursal
     */
    public function addSucursal(\Digital\GotitBundle\Entity\Sucursal $sucursales) {
        $this->sucursales[] = $sucursales;

        return $this;
    }

    /**
     * Remove sucursales
     *
     * @param \Digital\GotitBundle\Entity\Sucursal $sucursales
     */
    public function removeSucursal(\Digital\GotitBundle\Entity\Sucursal $sucursales) {
        $this->sucursales->removeElement($sucursales);
    }

    /**
     * Get sucursales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSucursals() {
        return $this->sucursales;
    }


    /**
     * Add beacons
     *
     * @param \Digital\GotitBundle\Entity\Beacon $beacons
     * @return Beacon
     */
    public function addBeacon(\Digital\GotitBundle\Entity\Beacon $beacons) {
        $this->beacons[] = $beacons;

        return $this;
    }

    /**
     * Remove beacons
     *
     * @param \Digital\GotitBundle\Entity\Beacon $beacons
     */
    public function removeBeacon(\Digital\GotitBundle\Entity\Beacon $beacons) {
        $this->beacons->removeElement($beacons);
    }

    /**
     * Get beacons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeacons() {
        return $this->beacons;
    }


    /**
     * Add productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     * @return Producto
     */
    public function addProducto(\Digital\GotitBundle\Entity\Producto $productos) {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     */
    public function removeProducto(\Digital\GotitBundle\Entity\Producto $productos) {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductos() {
        return $this->beacons;
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Add sucursales
     *
     * @param \Digital\GotitBundle\Entity\Sucursal $sucursales
     * @return Comercio
     */
    public function addSucursale(\Digital\GotitBundle\Entity\Sucursal $sucursales)
    {
        $this->sucursales[] = $sucursales;
    
        return $this;
    }

    /**
     * Remove sucursales
     *
     * @param \Digital\GotitBundle\Entity\Sucursal $sucursales
     */
    public function removeSucursale(\Digital\GotitBundle\Entity\Sucursal $sucursales)
    {
        $this->sucursales->removeElement($sucursales);
    }

    /**
     * Get sucursales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSucursales()
    {
        return $this->sucursales;
    }
}