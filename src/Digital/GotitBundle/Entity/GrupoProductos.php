<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrupoProductos
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Digital\GotitBundle\Entity\GrupoProductosRepository")
 */
class GrupoProductos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    //agrego relacion
    /**
     * @ORM\ManyToOne(targetEntity="Digital\GotitBundle\Entity\Comercio", inversedBy="gruposproducto")
     * @ORM\JoinColumn(name="comercio_id", referencedColumnName="id")
     */
    protected $comercio;

    /**
     * @ORM\ManyToMany(targetEntity="Producto", mappedBy="grupoproductos")
     * */
    protected $productos;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sucursal", inversedBy="grupoProductos")
     * @ORM\JoinColumn(name="sucursal_id", referencedColumnName="id")
     */
    protected $sucursal;
    
    /**
     * @ORM\OneToOne(targetEntity="Beacon", inversedBy="grupoProducto")
     * @ORM\JoinColumn(name="beacon_id", referencedColumnName="id", nullable=true)
     */
    protected $beacon;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return GrupoProductos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set comercio
     *
     * @param \Digital\GotitBundle\Entity\Comercio $comercio
     * @return GrupoProductos
     */
    public function setComercio(\Digital\GotitBundle\Entity\Comercio $comercio = null)
    {
        $this->comercio = $comercio;
    
        return $this;
    }

    /**
     * Get comercio
     *
     * @return \Digital\GotitBundle\Entity\Comercio 
     */
    public function getComercio()
    {
        return $this->comercio;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     * @return GrupoProductos
     */
    public function addProducto(\Digital\GotitBundle\Entity\Producto $productos)
    {
        $this->productos[] = $productos;
    
        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Digital\GotitBundle\Entity\Producto $productos
     */
    public function removeProducto(\Digital\GotitBundle\Entity\Producto $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Set sucursal
     *
     * @param \Digital\GotitBundle\Entity\Sucursal $sucursal
     * @return GrupoProductos
     */
    public function setSucursal(\Digital\GotitBundle\Entity\Sucursal $sucursal = null)
    {
        $this->sucursal = $sucursal;
    
        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \Digital\GotitBundle\Entity\Sucursal 
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * Set beacon
     *
     * @param \Digital\GotitBundle\Entity\Beacon $beacon
     * @return GrupoProductos
     */
    public function setBeacon(\Digital\GotitBundle\Entity\Beacon $beacon = null)
    {
        $this->beacon = $beacon;
    
        return $this;
    }

    /**
     * Get beacon
     *
     * @return \Digital\GotitBundle\Entity\Beacon 
     */
    public function getBeacon()
    {
        return $this->beacon;
    }
}