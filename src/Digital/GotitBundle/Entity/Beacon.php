<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Beacon
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Beacon
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=500)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="macaddress", type="string", length=250)
     */
    private $macaddress;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    //agrego relacion
    /**
     * @ORM\ManyToOne(targetEntity="Digital\GotitBundle\Entity\Comercio", inversedBy="beacons")
     * @ORM\JoinColumn(name="comercio_id", referencedColumnName="id")
     */
    protected $comercio;
    
    /**
     * @ORM\OneToOne(targetEntity="GrupoProductos")
     */
    protected $grupoProductos;
    
    /**
     * @ORM\OneToOne(targetEntity="Notificacion")
     */
    protected $notificacion;

    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $comercio_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Beacon
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set macaddress
     *
     * @param string $macaddress
     * @return Beacon
     */
    public function setMacaddress($macaddress)
    {
        $this->macaddress = $macaddress;

        return $this;
    }

    /**
     * Get macaddress
     *
     * @return string 
     */
    public function getMacaddress()
    {
        return $this->macaddress;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Beacon
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set comercio_id
     *
     * @param integer $comercioId
     * @return Beacon
     */
    public function setComercioId($comercioId)
    {
        $this->comercio_id = $comercioId;
    
        return $this;
    }

    /**
     * Get comercio_id
     *
     * @return integer 
     */
    public function getComercioId()
    {
        return $this->comercio_id;
    }

    /**
     * Set comercio
     *
     * @param \Digital\GotitBundle\Entity\Comercio $comercio
     * @return Beacon
     */
    public function setComercio( \Digital\GotitBundle\Entity\Comercio $comercio)
    {
        $this->comercio = $comercio;

        return $this;
    }

    /**
     * Get comercio
     *
     * @return \Digital\GotitBundle\Entity\Comercio
     */
    public function getComercio()
    {
        return $this->comercio;
    }

    /**
     * Set grupoProductos
     *
     * @param \Digital\GotitBundle\Entity\GrupoProductos $grupoProductos
     * @return Beacon
     */
    public function setGrupoProductos(\Digital\GotitBundle\Entity\GrupoProductos $grupoProductos = null)
    {
        $this->grupoProductos = $grupoProductos;
    
        return $this;
    }

    /**
     * Get grupoProductos
     *
     * @return \Digital\GotitBundle\Entity\GrupoProductos 
     */
    public function getGrupoProductos()
    {
        return $this->grupoProductos;
    }

    /**
     * Set notificacion
     *
     * @param \Digital\GotitBundle\Entity\Notificacion $notificacion
     * @return Beacon
     */
    public function setNotificacion(\Digital\GotitBundle\Entity\Notificacion $notificacion = null)
    {
        $this->notificacion = $notificacion;
    
        return $this;
    }

    /**
     * Get notificacion
     *
     * @return \Digital\GotitBundle\Entity\Notificacion 
     */
    public function getNotificacion()
    {
        return $this->notificacion;
    }
}