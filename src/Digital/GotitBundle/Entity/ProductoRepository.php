<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\EntityRepository;


class ProductoRepository extends EntityRepository {

    static $productosCollection;


    public function getProductos($comercioId = 0) {


        $dql = 'SELECT P.id, C.id as comercioId, P.nombre, P.codigo, P.cantidad, P.precio, P.imagenProducto '
            . 'FROM DigitalGotitBundle:Producto P '
            . 'INNER JOIN P.comercio C ';


        if ($comercioId != 0) {
            $dql = $dql . 'WHERE P.comercio = :comercioId ';
        }

        $dql = $dql . 'ORDER BY P.nombre ASC';

        $query = $this->getEntityManager()->createQuery($dql);
        if ($comercioId != 0) {
            $query->setParameter(':comercioId', $comercioId);
        }

        $results = $query->getResult();
        // var_dump($results);die;
        $collection = array();
        foreach ($results as $key => $value) {
            $filaValor = array('id' => $value['id'], 'comercio_id' => $value['comercioId'], 'nombre' => $value['nombre'], 'codigo' => $value['codigo'], 'cantidad' => $value['cantidad'], 'precio' => $value['precio'], 'imagenProducto' => $value['imagenProducto']);

            $collection[] = $filaValor;
        }
        unset($results);
        self::$productosCollection = $collection;


        return self::$productosCollection;
    }

    public function getProductosPorComercio($cadenaId = 0) {

        $productos = $this->getProductos($cadenaId);

        return $productos;
    }



}
