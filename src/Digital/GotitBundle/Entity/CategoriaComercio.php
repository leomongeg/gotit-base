<?php

namespace Digital\GotitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CategoriaComercio
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CategoriaComercio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icono;

    /**
     * @ORM\OneToMany(targetEntity="Comercio", mappedBy="categoriaComercio")
     */
    protected $comercios;


    /**
     * Constructor
     */
    public function __construct() {
        $this->comercios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CategoriaComercio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set icono
     *
     * @param string $icono
     * @return CategoriaComercio
     */
    public function setIcono($icono)
    {
        $this->icono = $icono;

        return $this;
    }

    /**
     * Get icono
     *
     * @return string 
     */
    public function getIcono()
    {
        return $this->icono;
    }


    /**
     * Add comercios
     *
     * @param \Digital\GotitBundle\Entity\Comercio $comercios
     * @return CategoriaComercio
     */
    public function addComercio(\Digital\GotitBundle\Entity\Comercio $comercios) {
        $this->comercios[] = $comercios;

        return $this;
    }

    /**
     * Remove comercios
     *
     * @param \Digital\GotitBundle\Entity\Comercio $comercios
     */
    public function removeComercio(\Digital\GotitBundle\Entity\Comercio $comercios) {
        $this->comercios->removeElement($comercios);
    }

    /**
     * Get comercios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComercios() {
        return $this->comercios;
    }

    public function __toString() {
        return $this->getNombre();
    }

}