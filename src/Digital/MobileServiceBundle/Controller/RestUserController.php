<?php

namespace Digital\MobileServiceBundle\Controller;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class RestUserController extends FOSRestController implements ClassResourceInterface {

    /**
     * Get the token
     * 
     * @Rest\Post("/user/get_token")
     * @Rest\View()
     */
    public function getTokenAction() 
    {
        //Authorization header
        //$this->add_header('Authorization', 'WSSE profile="UsernameToken"');
        //X-WSSE header
        // Parametros de usuario y clave
        $username = $this->getRequest()->get('username');
        $password = $this->getRequest()->get('password');

        // Traer la sal del usuario
        $userManager = $this->getDoctrine()->getManager('default')->getRepository('ApplicationSonataUserBundle:User');
        $user = $userManager->findOneByUsername($username);
        if (is_null($user))
            throw new UnauthorizedHttpException('X-WSSE', 'Usuario o contraseña invalidos.');

        // Utilizar el encoder de usuario para crear nuevamente la clave
        $encoder_factory = $this->get('security.encoder_factory');
        $userEncoder = $encoder_factory->getEncoder($user);
        // Genero la clave de usuario
        $secret = $userEncoder->encodePassword($password, $user->getSalt());
        // $secret = $password;
        // $created = date('r'); //$created = date('d/M/Y H:i:s T');
        $created = date('Y-m-d H:i:s');
        $nonce = uniqid();


        // Se valida el password encriptado para ver si son iguales.
        if ($user->getPassword() != $secret)
            throw new UnauthorizedHttpException('X-WSSE', 'Usuario o contraseña invalidos.');

        // Crea el token
        $encoder = new MessageDigestPasswordEncoder('sha1', true, 1);

        $digest = $encoder->encodePassword(
                sprintf(
                        '%s%s%s', base64_decode($nonce), $created, $secret
                ), $user->getSalt()
        );

        // Headers de usuario
        $x_wsse = sprintf('UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"', $username, $digest, $nonce, $created);
        $entities = array('XWSSE' => $x_wsse, 'Authorization' => 'WSSE profile="UsernameToken"');

        return array(
            'entities' => $entities,
        );
    }
    
    /**
     * Registro de usuarios
     * 
     * @Rest\Post("/user/registration")
     * @Rest\View()
     */
    public function userRegistrationAction()
    {
        $user = new \Application\Sonata\UserBundle\Entity\User();
        $form = $this->createForm(new \Digital\MobileServiceBundle\Form\Type\userRegistrationForm($this->getDoctrine()), $user);
        
        $errors = array();
        if (!$user = $this->processForm($form, $errors))
        {
            return array('msn' => 'Error', "info" => $errors);
        }
        $user->setUsername($user->getEmail());
        $user->setPassword($user->getPlainPassword());
        $user->setEnabled(true);
        $em        = $this->getDoctrine()->getManager();
//        $grupoRepo = $em->getRepository('ApplicationSonataUserBundle:Group');
//        $grupo     = $grupoRepo->findOneBy(array('name' => 'User'));
//        $user->addGroup($grupo);
        
//        $this->checkFacebookProfilePicture($user);
        $em->persist($user);
        $em->flush();
        $repo = $em->getRepository('ApplicationSonataUserBundle:User');

        return array(
            'msn'  => 'OK',
            'info' => $repo->getUserForWSResponse($user->getId())
        );
    }
    
    /**
     * Procesa el formulario para ver si hay errores en los datos enviados.
     * 
     * @param array $errors Si hay errores se inyectan en esta variable.
     * @return mixed El objeto User con los datos provenientes del request O FALSE 
     * si hay errores en los datos provenientes del request.
     */
    private function processForm($_form, &$errors = null)
    {
        $_form->submit($this->getRequest());

        if ($_form->isValid())
        {
            return $_form->getData();
        }

        $childrens = $_form->createView()->children;
        $errors = $this->formatFormErrors($childrens);

        //$errors = $childrens;

        return false;
    }
    
    /**
     * Convierte los errores del formulario en un arreglo para poder retornarlos
     * mediante rest.
     * 
     * @param type $childrens
     * @param type $errors
     * @return type
     */
    private function formatFormErrors($childrens = array(), $errors = array())
    {
        foreach ($childrens as $key => $child)
        {
            foreach ($child->vars['errors'] as $key => $error)
            {
                $errors[$child->vars['name']][] = $error->getMessage();
            }
        }

        return $errors;
    }
    
    /**
     * Verifica si el usuario referenciado tiene facebook id y si lo tiene, procede
     * a descargar la imagen de perfil correspondiente, la almacena en la carpeta de
     * fotos de perfil y la asigna al objeto usuario referenciado.
     * 
     * @param \Application\Sonata\UserBundle\Entity\User $user
     */
    private function checkFacebookProfilePicture(User $user)
    {
        if (is_null($user->getFile()) && ($user->getFacebookUid() != '' || !is_null($user->getFacebookUid())))
        {
            $filename = sha1(uniqid(mt_rand(), true)) . '.jpg';
            $fbPhotoUrl = sprintf("http://graph.facebook.com/%s/picture?type=large", $user->getFacebookUid());
//            $fileDestination = $user->getUploadRootDir() . '/' . $filename;

//            if (@copy($fbPhotoUrl, $fileDestination))
//            {
//                $user->setPath($filename);
//                $user->fotoUrl = 'http://' . $this->getRequest()->getHost() . '/' . $user->getWebPath();
//            }
        }
    }

}
