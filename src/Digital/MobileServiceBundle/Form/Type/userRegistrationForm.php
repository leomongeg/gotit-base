<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Digital\MobileServiceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Description of userRegistrationForm
 *
 * @author leonardo
 */
class userRegistrationForm extends AbstractType
{
    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @return Registry
     *
     * @throws \LogicException If DoctrineBundle is not available
     */
    public function getDoctrine() {
        return $this->em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname')
                ->add('lastname')
                ->add('email')
                ->add('plainPassword')
                ->add('dateOfBirth')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => 'Application\Sonata\UserBundle\Entity\User',
            'constraints' => array(new Assert\Callback(array(array($this, 'validarUniqueFields'))))
        ));
    }

    public function validarUniqueFields($object, \Symfony\Component\Validator\ExecutionContextInterface $context)
    {
        if (!$this->isUniqueRequired('email', $object->getEmail()))
            $context->addViolationAt('email', sprintf('El correo %s ya se encuentra en uso', $object->getEmail()), array(), $object->getEmail(), null, 0);

        if (!$this->isUniqueNotRequired('facebookUid', $object->getFacebookUid()))
            $context->addViolationAt('facebookUid', sprintf('El facebook_uid %s ya se encuentra en uso', $object->getFacebookUid()), array(), $object->getFacebookUid(), null, 0);
    }

    /**
     * Verifica si el $value de $field se encuentra registrado en base de datos.
     * 
     * @param string $field
     * @param string $value
     * @return boolean
     */
    private function isUniqueRequired($field, $value)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository("ApplicationSonataUserBundle:User");
        $user = $repo->findOneBy(array($field => $value));

        return $user ? false : true;
    }

    /**
     * Si el valor de $field viene en el formulario. Si viene, se verifica si hay algun
     * registro con el mismo valor de $field.
     * 
     * @param string $field Propiedad a validar
     * @param string $value Valore a buscar
     * @return boolean
     */
    private function isUniqueNotRequired($field, $value)
    {
        if (ctype_space($value) || $value != '')
        {
            $repo = $this->getDoctrine()->getManager()->getRepository("ApplicationSonataUserBundle:User");
            $user = $repo->findOneBy(array($field => $value));

            return $user ? false : true;
        }

        return true;
    }

    public function getName()
    {
        return "";
    }

}
