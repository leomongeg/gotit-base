<?php

namespace Digital\ImportsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends DigitalController
{
    /**
     * @Route("/hello")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
